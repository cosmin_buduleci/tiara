import { IOSCustomTransition } from './../config/ios-custom-transition'

import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicApp, IonicModule, IonicErrorHandler, Config } from 'ionic-angular';
import { TiaraApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ModalApptPage } from '../pages/modal-appt/modal-appt';
import { ModalContactImportPage } from '../pages/modal-contact-import/modal-contact-import';
import { ModalContactNewPage } from '../pages/modal-contact-new/modal-contact-new';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';
import { ShortcutMenuComponent } from '../components/shortcut-menu/shortcut-menu';
import { DashboardProvider } from '../providers/dashboard/dashboard';

import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth'
import { FirebaseProvider } from './../providers/firebase/firebase';
import { AppointmentsProvider } from '../providers/appointments/appointments';

const firebaseConfig = {
  apiKey: "AIzaSyACo0TA5K_nBcEjyS036kmG_ob0bbZwGFs",
  authDomain: "tiara-style.firebaseapp.com",
  databaseURL: "https://tiara-style.firebaseio.com",
  projectId: "tiara-style",
  storageBucket: "tiara-style.appspot.com",
  messagingSenderId: "984656056202"
};

@NgModule({
  declarations: [
    TiaraApp,
    HomePage,
    LoginPage,
    SignupPage,
    DashboardPage,
    ModalApptPage,
    ModalContactImportPage,
    ModalContactNewPage,
    ShortcutMenuComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(TiaraApp),
    HttpModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    TiaraApp,
    HomePage,
    LoginPage,
    SignupPage,
    DashboardPage,
    ModalApptPage,
    ModalContactImportPage,
    ModalContactNewPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FirebaseProvider,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthProvider,
    DashboardProvider,
    AppointmentsProvider
  ]
})
export class AppModule {

  constructor(config: Config) {

    config.setTransition('ios-transition', IOSCustomTransition);

  }

}
