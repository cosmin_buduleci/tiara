import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Parse } from 'Parse';

import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';

@Component({
  templateUrl: 'app.html'
})
export class TiaraApp {
  rootPage: any = LoginPage;
  // rootPage: any = DashboardPage;

  constructor(platform: Platform, private statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {

      // let status bar overlay webview
      this.statusBar.overlaysWebView(false);

      // set status bar style
      this.statusBar.styleLightContent();

      // set status bar color
      this.statusBar.backgroundColorByHexString('#313130');

      splashScreen.hide();
    });
  }
}
