import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { SignupPage } from '../signup/signup';
import { DashboardPage } from '../dashboard/dashboard';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  signupPage = SignupPage;

  loginData: loginForm;
  errorsList: any;
  showError: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private Auth: AuthProvider
  ) {
    this.loginData = {
      email: '',
      password: ''
    }
    this.errorsList = {
      '200': 'Contul nu a fost validat. Te rugăm să verifici email-ul pentru a valida contul.',
      '201': 'Email-ul este invalid.',
      '202': 'Email-ul nu a fost introdus.',
      '203': 'Parola nu a fost introdusă, minim 6 caractere.',
      '204': 'Parola de confirmare nu a fost introdusă, minim 6 caractere.',
      '205': 'Parola nu corespunde cu parola de confirmare.'
    };
    // reset errors
    this.showError = [];
  }

  // login with email and password
  login() {
    let errors = [];

    //  validate email
    if (this.loginData.email.length < 1) {
      errors.push(this.errorsList['202']);
    } else if (!this.validateEmail(this.loginData.email)) {
      errors.push(this.errorsList['201']);
    }

    // validate password
    if (this.loginData.password.length < 6) {
      errors.push(this.errorsList['203']);
    }

    // ok -> send data to server
    if (!errors.length) {
      this.Auth.login(this.loginData).then((user: any) => {
        if (!user.emailVerified) {
          this.showError.push(this.errorsList['200']);
          this.Auth.logout();
        }
      }).catch(error => {
        console.log(error);
      });
    }

    // push errors
    this.showError = errors;

  }

  // login with facebook
  loginWithFB() {
    // login with facebook
    this.Auth.loginWithFacebook()
      .then((res) => {
        console.log('gooo');
      })
      .catch((err) => console.log(err));
  }

  // regex for email
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

}

interface loginForm {
  email: string;
  password: string;
}