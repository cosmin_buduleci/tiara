import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';

/**
 * Generated class for the SignupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  signupData: formSignup;
  rePassword: string;
  errorsList: any;
  showError: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private Auth: AuthProvider
  ) {
    this.signupData = {
      email: '',
      password: ''
    }
    this.errorsList = {
      'auth/email-already-in-use': 'Acest email există în baza de date.',
      '201': 'Email-ul este invalid.',
      '202': 'Email-ul nu a fost introdus.',
      '203': 'Parola nu a fost introdusă, minim 6 caractere.',
      '204': 'Parola de confirmare nu a fost introdusă, minim 6 caractere.',
      '205': 'Parola nu corespunde cu parola de confirmare.'
    };
    this.rePassword = '';
    // reset errors
    this.showError = [];
  }

  // Signup
  signup() {
    let errors = [];

    //  validate email
    if (this.signupData.email.length < 1) {
      errors.push(this.errorsList['202']);
    } else if (!this.validateEmail(this.signupData.email)) {
      errors.push(this.errorsList['201']);
    }

    // validate password
    if (this.signupData.password.length < 6) {
      errors.push(this.errorsList['203']);
    } else if (this.rePassword.length < 1) {
      errors.push(this.errorsList['204']);
    } else if (this.signupData.password !== this.rePassword) {
      errors.push(this.errorsList['205']);
    }

    // ok -> send data to server
    if (!errors.length) {
      this.Auth.signup(this.signupData).then((user: any) => {
        // reset inputs
        this.signupData = {
          email: '',
          password: ''
        }
        this.rePassword = '';
        // alert email validation
        const alert = this.alertCtrl.create({
          title: 'Perfect!',
          subTitle: 'Te rugăm să verifici email-ul pentru a valida contul.',
          buttons: ['Ok']
        });
        alert.present();
        this.navCtrl.push(LoginPage);
      }).catch(error => {
        errors.push(this.errorsList[error.code]);
        console.log(error);
        // push errors
        this.showError = errors;
      });
    }

    // push errors
    this.showError = errors;

  }

  // regex for email
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

}

interface formSignup {
  email: string;
  password: string;
}

