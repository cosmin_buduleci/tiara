import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { DashboardProvider } from '../../providers/dashboard/dashboard';

/**
 * Generated class for the ClientsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-clients',
  templateUrl: 'clients.html',
})
export class ClientsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private _dashProvider: DashboardProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientsPage');
  }

  newClient() {
    let data = {
      open: true,
      type: 'new-client',
      options: {}
    }
    this._dashProvider.openModal(data);
  }

  importClients() {
    let data = {
      open: true,
      type: 'import-client',
      options: {}
    }
    this._dashProvider.openModal(data);
  }

}
