import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SchedulerPage } from './scheduler';

import { CalendarComponent } from './../../components/calendar/calendar';

@NgModule({
  declarations: [
    SchedulerPage,
    CalendarComponent
  ],
  imports: [
    IonicPageModule.forChild(SchedulerPage),
  ],
  exports: [
    SchedulerPage
  ]
})
export class SchedulerPageModule {}
