import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalContactNewPage } from './modal-contact-new';

@NgModule({
  declarations: [
    ModalContactNewPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalContactNewPage),
  ],
  exports: [
    ModalContactNewPage
  ]
})
export class ModalContactNewPageModule {}
