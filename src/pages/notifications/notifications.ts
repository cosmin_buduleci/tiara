import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';

/**
 * Generated class for the NotificationsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

  notificationsData: any;

  constructor(
    public navCtrl: NavController, 
    private firebase: FirebaseProvider,
    public navParams: NavParams
  ) {
  
    this.notificationsData = [
      {
        client: 'Cosmin',
        date: '10.08.2017',
        hour: 12,
        minute: 45,
        service: 'Tunsoare baieti'
      },
      {
        client: 'Alex',
        date: '12.08.2017',
        hour: 10,
        minute: 15,
        service: 'Tunsoare baieti'
      }
    ]
  
  }

  confirm() {
    this.firebase.addItem('abracadabra');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationsPage');
  }

}
