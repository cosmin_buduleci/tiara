import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalApptPage } from './modal-appt';

@NgModule({
  declarations: [
    ModalApptPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalApptPage),
  ],
  exports: [
    ModalApptPage
  ]
})
export class ModalApptPageModule {}
