import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Select } from 'ionic-angular';


/**
 * Generated class for the ModalApptPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-modal-appt',
  templateUrl: 'modal-appt.html',
})
export class ModalApptPage {

  @ViewChild('duration') duration: Select;

  data: any;
  minDate: any;
  minutesVal: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    let today = new Date();
    today.setUTCHours(0);
    this.minDate = (new Date(today)).toISOString();

    let params = {
      time: navParams.get('time'),
      date: navParams.get('date')
    }
    console.log(params);

    this.data = {
      type: 'client',
      time: '10:30',
      date: this.minDate,
      duration: '00:30'
    }

    // change with params detail
    if (params.time) {
      this.data.time = params.time;
    }
    if (params.date) {
      this.data.date = params.date;
    }

    this.minutesVal = this.generateMinutes();

  }

  // generate minutes
  generateMinutes() {
    let minutesLength = 60 / 5;
    let i;
    let minutes = [];
    for (i = 1; i < minutesLength; i++) {
      minutes.push(i*5);
    }
    return minutes.join(',');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalApptPage');
  }

  // close modal
  closeModal() {
    this.navCtrl.pop();
  }

  // save appt
  saveAppt() {
    console.log(this.data);
  }

}
