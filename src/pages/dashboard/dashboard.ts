import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Tabs, ModalController } from 'ionic-angular';

import { AnalyticsPage } from '../analytics/analytics';
import { NotificationsPage } from '../notifications/notifications';
import { SchedulerPage } from '../scheduler/scheduler';
import { ClientsPage } from '../clients/clients';
import { SettingsPage } from '../settings/settings'

import { ModalApptPage } from '../../pages/modal-appt/modal-appt';
import { ModalContactImportPage } from '../../pages/modal-contact-import/modal-contact-import';
import { ModalContactNewPage } from '../../pages/modal-contact-new/modal-contact-new';

import { DashboardProvider } from '../../providers/dashboard/dashboard';
import { Subscription } from 'rxjs/Subscription';

import * as moment from 'moment';
import 'moment/locale/ro';

/**
 * Generated class for the DashboardPage tabs.
 *
 * See https://angular.io/docs/ts/latest/guide/dependency-injection.html for
 * more info on providers and Angular DI.
 */
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
@IonicPage(
  {
    name: 'page-dashboard'
  }
)
export class DashboardPage {

  @ViewChild('dashTabs') dashTabs: Tabs;

  analyticsRoot = 'AnalyticsPage';
  notificationsRoot = 'NotificationsPage';
  schedulerRoot = 'SchedulerPage';
  clientsRoot = 'ClientsPage';
  settingsRoot = 'SettingsPage';

  subscriptionModal: Subscription;

  constructor(
    public navCtrl: NavController,
    private _dashProvider: DashboardProvider,
    public modalCtrl: ModalController
  ) {


    // watch day for change
    let timeoutId;
    let currentDay;
    let actualDay;
    function watchDay() {
      actualDay = moment().day();
      if (currentDay !== actualDay) {
        currentDay = actualDay;
        // emit event hear!!!
        console.log('changed day')
        _dashProvider.changedDay(currentDay);
      }
      console.log(currentDay);
      timeoutId = setTimeout(() => {
        clearTimeout(timeoutId);
        watchDay();
      }, 10000);
    }
    watchDay();

  }

  ionViewDidEnter() {
    this.dashTabs.select(2);
  }

  openModal(data) {
    let options = data.options;

    if (data.type === 'new-appt') {
      this.modalCtrl.create(ModalApptPage, options).present();
    } 
    else if (data.type === 'new-client') {
      this.modalCtrl.create(ModalContactNewPage, options).present();
    } 
    else if (data.type === 'import-client') {
      this.modalCtrl.create(ModalContactImportPage, options).present();
    }
  }

  ngOnInit() {
    this.subscriptionModal = this._dashProvider.openModal$
      .subscribe(data => {
        this.openModal(data);
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component is destroyed
    this.subscriptionModal.unsubscribe();
  }

}
