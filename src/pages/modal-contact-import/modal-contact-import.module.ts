import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalContactImportPage } from './modal-contact-import';

@NgModule({
  declarations: [
    ModalContactImportPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalContactImportPage),
  ],
  exports: [
    ModalContactImportPage
  ]
})
export class ModalContactImportPageModule {}
