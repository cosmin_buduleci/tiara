import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';

/**
 * Generated class for the ModalContactImportPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-modal-contact-import',
  templateUrl: 'modal-contact-import.html',
  providers: [Contacts]
})
export class ModalContactImportPage {

  contactsData: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private contacts: Contacts) {

    contacts.find(["*"]).then((data) => {
      console.log(data);
    });

    this.contactsData = [
      {
        id: '221dewg43',
        name: 'Cosmin',
        selected: false
      },
      {
        id: '221dewg43',
        name: 'Cosmin',
        selected: false
      },
      {
        id: '221dewg43',
        name: 'Cosmin',
        selected: false
      }
    ];

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalContactImportPage');
  }

  // close modal
  closeModal() {
    this.navCtrl.pop();
  }

}
