import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';

/*
  Generated class for the DashboardProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class DashboardProvider {

  constructor(public http: Http) {
    console.log('Hello DashboardProvider Provider');
  }

  // Observable day changed
  private _dayChanged = new BehaviorSubject<any>(0);
  changedDay$ = this._dayChanged.asObservable();
  
  changedDay(day) {
    this._dayChanged.next(day);
  }

  // open appt modal
  private _modal = new BehaviorSubject<any>(0);
  openModal$ = this._modal.asObservable();
  
  openModal(data) {
    this._modal.next(data);
  }

}
