import { Component, Input, ViewChild } from '@angular/core';
import { Content, Slides } from 'ionic-angular';
import * as moment from 'moment';
import 'moment/locale/ro';
import { DashboardProvider } from '../../providers/dashboard/dashboard';
import { Subscription } from 'rxjs/Subscription';



@Component({
  selector: 'calendar',
  templateUrl: 'calendar.html',
  host: {
    '(window:resize)': 'calculateStaffPerSlide($event)'
  }
})
export class CalendarComponent {

  @ViewChild(Content) content: Content;
  @ViewChild('sliderCalendar') slides: Slides;
  @ViewChild('workerStaff') staffSlides: Slides;

  // define variables types
  text: string;
  apptsTest: any;
  hours: hoursFormat[];
  toggleDays: boolean;
  groupAppts: any;
  day: any;
  loadComplete: boolean;
  date: any;
  daysList: any;
  subscription: Subscription;
  item: number;
  maxStaffPerSlide: number;
  staff: any;
  tPic: string = 'https://odesk-prod-portraits.s3.amazonaws.com/Users:b_cosmin:PortraitUrl_100?AWSAccessKeyId=1XVAX3FNQZAFC9GJCFR2&Expires=2147483647&Signature=MnftPXH1IaUrE5Lqt9EyC0nBIKE%3D&1501335836865757&h61VF';

  constructor(private _dashProvider: DashboardProvider) {

    this.staff = [{}, {}, {}, {}, {}, {}];

    this.day = moment().calendar(null, {
      sameDay: '[Astăzi]',
      nextDay: '[Mâine]',
      nextWeek: 'dddd',
      lastDay: '[Ieri]',
      sameElse: 'dddd'
    });

    this.date = moment().format('DD.MM.YYYY');

    this.getDays();

    this.apptsTest = [
      {
        title: 'Apple',
        description: 'Descriere de cateva caractere optionala.',
        type: 'client',
        start: {
          hour: 2,
          minute: 30
        },
        duration: 45
      },
      {
        title: 'Apple',
        description: 'Descriere de cateva caractere optionala.',
        type: 'client',
        start: {
          hour: 1,
          minute: 30
        },
        duration: 35
      },
      {
        title: 'Apple',
        description: 'Descriere de cateva caractere optionala.',
        type: 'client',
        start: {
          hour: 1,
          minute: 30
        },
        duration: 30
      },
      {
        title: 'Apple',
        description: '',
        type: 'personal',
        start: {
          hour: 2,
          minute: 0
        },
        duration: 45
      },
      {
        title: 'Apple',
        description: 'Descriere de cateva caractere optionala.',
        type: 'client',
        start: {
          hour: 13,
          minute: 0
        },
        duration: 45
      },
      {
        title: 'Apple',
        description: 'Descriere de cateva caractere optionala.',
        type: 'client',
        start: {
          hour: 14,
          minute: 15
        },
        duration: 78
      }
      ,
      {
        title: 'Apple',
        description: 'Descriere de cateva caractere optionala.',
        type: 'client',
        start: {
          hour: 14,
          minute: 47
        },
        duration: 45
      }
    ];

    // call get hours function
    this.hours = this.getHours();

    this.groupAppts = this.organizeAppts(this.apptsTest);

    setTimeout(() => {
      this.calculateStaffPerSlide();
    }, 300)

  }

  // calculate staff per slide 
  calculateStaffPerSlide() {

    let staff = this.staff;
    let widthScreen = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    let maxSlides = Math.floor(widthScreen / 300);
    let staffLength = staff.length;

    if (staffLength > maxSlides) {
      this.maxStaffPerSlide = maxSlides;
    } else {
      this.maxStaffPerSlide = staffLength;
    }

    this.resetSlider();

  }

  resetSlider() {
    this.loadComplete = true;
    if (this.slides && this.staffSlides) {
      this.slides.control = this.staffSlides;
      this.staffSlides.control = this.slides;
      this.slides.update();
      this.staffSlides.update();
    } else {
      setTimeout(() => {
        this.resetSlider();
      }, 300);
    }
  }

  // change day
  changeDay(data) {
    this.day = data.day;
    this.date = data.date;
    this.toggleDays = false;
  }

  // get days list
  getDays() {
    console.log(moment().day())

    let i;
    let startDay = moment().day();
    let days = 10;
    this.daysList = [];

    for (i = 0; i < days; i++) {

      let currentDay = moment().day() + i;

      let day = moment().day(currentDay).calendar(null, {
        sameDay: '[Astăzi]',
        nextDay: '[Mâine]',
        nextWeek: 'dddd',
        lastDay: '[Ieri]',
        sameElse: 'dddd'
      });

      let date = moment().day(currentDay).format('DD.MM.YYYY');

      this.daysList.push({ day: day, date: date });

    }
  }

  // get hours objects
  getHours() {

    const totalHours = 24 * 4;

    let hoursInput = [];

    let i;
    let hour;
    let minute;
    let currentHour = {};

    for (i = 0; i < totalHours; i++) {

      hour = Math.floor(i * 15 / 60);
      minute = Math.floor(i * 15 - (Math.floor(i * 15 / 60) * 60));

      currentHour = {
        hour: hour,
        minute: minute
      }

      hoursInput.push(currentHour);

    }

    return hoursInput;

  };

  reOrganize() {
    this.groupAppts = this.organizeAppts(this.apptsTest);
  }

  // appointments organize
  organizeAppts(appts) {

    let i;
    let j;

    let lengthAppts = appts.length;

    let groupAppts = [];
    let groupNr = 1;

    // first step => sorting by start date
    function compare(a, b) {
      if (a.start.hour * 60 + a.start.minute < b.start.hour * 60 + b.start.minute)
        return -1;
      if (a.start.hour * 60 + a.start.minute > b.start.hour * 60 + b.start.minute)
        return 1;
      return 0;
    }

    appts.sort(compare);

    // step 2 => group by colide appts
    for (i = 0; i < lengthAppts; i++) {
      for (j = i + 1; j < lengthAppts; j++) {

        if (i !== j) {

          // get start and end point value
          let startOB1 = (appts[i].start.hour * 60) + appts[i].start.minute;
          let startOB2 = (appts[j].start.hour * 60) + appts[j].start.minute;

          let endOB1 = startOB1 + appts[i].duration;
          let endOB2 = startOB2 + appts[j].duration;

          // do stuff

          if (startOB1 < startOB2) {

            if ((endOB1 - startOB2) > 0) {

              if (!appts[i].group && !appts[j].group) {
                appts[i].group = groupNr;
                appts[j].group = groupNr;
                groupNr++;
              } else if (appts[i].group) {
                appts[j].group = appts[i].group;
              } else if (appts[j].group) {
                appts[i].group = appts[j].group;
              }

            }

          } else {

            if ((endOB2 - startOB1) > 0) {

              if (!appts[i].group && !appts[j].group) {
                appts[i].group = groupNr;
                appts[j].group = groupNr;
                groupNr++;
              } else if (appts[i].group) {
                appts[j].group = appts[i].group;
              } else if (appts[j].group) {
                appts[i].group = appts[j].group;
              }

            }

          }

        }

      }
    }

    // step 3 => push appts to specific group
    for (i = 0; i < lengthAppts; i++) {

      if (!appts[i].group) {
        appts[i].group = groupNr;
        groupNr++;
      }

      let indexGroup = appts[i].group - 1;

      if (!groupAppts[indexGroup]) {
        groupAppts[indexGroup] = [];
      }

      groupAppts[indexGroup].push(appts[i]);

    }

    // return group appts
    return groupAppts;

  };

  changedDate(day) {
    console.log('ziua: ', day)
  }

  // add appt
  addAppt(hour, date) {
    let dateSplit = date.split('.');

    let data = {
      open: true,
      type: 'new-appt',
      options: {
        time: ("00" + hour.hour).slice(-2) + ':' + ("00" + hour.minute).slice(-2),
        date: dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0]
      }
    }
    this._dashProvider.openModal(data);
  }

  ngOnInit() {
    this.subscription = this._dashProvider.changedDay$
      .subscribe(day => this.changedDate(day));
  }

  // scroll to specific hour(ex: 'el' + hour + minute, like el120, el125)
  scrollTo(element: string) {
    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 300);
  }

  ngOnDestroy() {
    // prevent memory leak when component is destroyed
    this.subscription.unsubscribe();
  }

}

interface hoursFormat {
  hour: number;
  minute: number;
}