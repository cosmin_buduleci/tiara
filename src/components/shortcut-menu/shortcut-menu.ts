import { Component } from '@angular/core';
import { DashboardProvider } from '../../providers/dashboard/dashboard';

/**
 * Generated class for the ShortcutMenuComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'shortcut-menu',
  templateUrl: 'shortcut-menu.html'
})
export class ShortcutMenuComponent {

  toggle: boolean;

  constructor(private _dashProvider: DashboardProvider) {
  }

  openMenu() {
    this.toggle = !this.toggle;
  }

  addAppt() {
    let data = {
      open: true,
      type: 'new-appt',
      options: {}
    }
    this._dashProvider.openModal(data);
  }

}
