import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ShortcutMenuComponent } from './shortcut-menu';

@NgModule({
  declarations: [
    ShortcutMenuComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    ShortcutMenuComponent
  ]
})
export class ShortcutMenuComponentModule {}
